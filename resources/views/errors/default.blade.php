@extends('theme::layouts.master-no-menu')
@inject('bookService', 'Modules\Admin\Services\LanguagesService')

@section('page_title')| 404 @stop

@section('meta')
    <meta name="description"
          content="{{ $bookService->translate('site.description', 'settings') }}"/>
    <meta name="keywords"
          content="{{ $bookService->translate('site.keywords', 'settings') }}"/>
@stop

@section('css')
@stop

@section('javascript')
    {{--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b8598fd3fd4a8df"></script>--}}
@stop
@section('content')
    <header class="masthead" style="height: 100vh;">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-12 my-auto">
                    <div class="header-content mx-auto">
                        @if(substr($exception, 0 , 2) == '50')
                            <h1 class="mb-5">{{ trans('errors.500') }}</h1>
                            <p class="text-center"> {{ trans('errors.500-description') }} </p>
                            <a href="{{ route('public.index') }}"
                               class="btn btn-outline js-scroll-trigger">{{ trans('errors.back-to-index') }}</a>
                        @else
                            <h1 class="mb-5">{{ trans('errors.404') }}</h1>
                            <p class="text-center"> {{ trans('errors.404-description') }} </p>
                            <a href="{{ route('public.index') }}"
                               class="btn btn-outline js-scroll-trigger">{{ trans('errors.back-to-index') }}</a>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </header>

    <section class="contact bg-primary" id="contact">
        <div class="container">
            <h2>{{ $bookService->translate('book::generic.share-this', 'translation', 'book.share-this') }}</h2>
            {!! setting('site.site-share-code') !!}
        </div>
    </section>
@stop
