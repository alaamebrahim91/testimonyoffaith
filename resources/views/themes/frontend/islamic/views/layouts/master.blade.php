<!DOCTYPE html>
<html lang="{{ LaravelLocalization::getCurrentLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @yield('meta')
    <title> {{ setting('site.title') }}  @yield('page_title')</title>
    <meta name="base_url" content="https://www.theguidetoislam.com/">
    <meta property="og:locale" content="{{ LaravelLocalization::getCurrentLocale() }}"/>
    <meta property="og:site_name" content="{{ setting('site.title') }}"/>
    <meta property="og:title" content="{{ setting('site.description') }}"/>
    <meta property="og:url" content="https://www.theguidetoislam.com"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image"
          content="https://theguidetoislam.com/storage/2xL1upS1tVYNkrAUyPO1sgXPO8CjOJqPEshDSZg5.jpeg"/>
    <meta property="og:description" content="{{ setting('site.description') }}"/>
    <meta name="twitter:title" content="{{ setting('site.description') }}"/>
    <meta name="twitter:url" content="https://theguidetoislam.com/"/>
    <meta name="twitter:site" content="@t2dialogue"/>
    <meta name="twitter:creator" content="@t2dialogue"/>
    <meta name="twitter:description" content="{{ setting('site.description') }}"/>
    <meta name="twitter:image"
          content="https://theguidetoislam.com/storage/2xL1upS1tVYNkrAUyPO1sgXPO8CjOJqPEshDSZg5.jpeg"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta itemprop="name" content="{{ setting('site.description') }}"/>
    <meta itemprop="description" content="{{ setting('site.description') }}"/>
    <meta itemprop="image" content="https://theguidetoislam.com/storage/2xL1upS1tVYNkrAUyPO1sgXPO8CjOJqPEshDSZg5.jpeg"/>

    @yield('main_css')

    {{-- main css --}}
    <link rel="stylesheet" href="{{ asset('assets/css/base.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    @if(in_array(LaravelLocalization::getCurrentLocale(), ['ar', 'ur', 'fa']) )
    <link rel="stylesheet" href="{{ asset('assets/css/rtl.css') }}"> @endif

    {{-- Main js --}}
    <script type="text/javascript" src="{{ asset('assets/js/modernizr.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pace.min.js') }}"></script>

    {{--<!-- Custom fonts for this template -->--}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    @yield('css')
</head>

<body id="top">

@yield('content')

<div id="preloader">
    <div id="loader">
        <div class="line-scale">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
</div>
{{--<!-- Bootstrap core JavaScript -->--}}
<script src="{{ asset('assets/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
@yield('javascript')
{!! setting('site.google_analytics_code') !!}
</body>
</html>
