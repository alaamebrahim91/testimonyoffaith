@inject('translationService', 'Modules\Admin\Services\LanguagesService')

<section class="s-pageheader s-pageheader--home">

    <header class="header">
        <div class="header__content row">

            <div class="header__logo">
                {{ setting('site.title') }}
            </div>

            <a class="header__toggle-menu" href="#0" title="Menu"><span>Menu</span></a>

            <nav class="header__nav-wrap">

                <h2 class="header__nav-heading h6">Site Navigation</h2>

                @if(in_array(LaravelLocalization::getCurrentLocale(), ['ar', 'ur', 'fa']) )
                    <ul class="header__nav" dir="rtl">
                @else <ul class="header__nav"> @endif
                <ul class="header__nav">
                    <li class="current"><a href="{{ route('public.index') }}" title="">{!! $translationService->translate('home::menu.home_url', 'translation') !!}</a></li>
                    <li><a href="style-guide.html" title="">{!! $translationService->translate('home::menu.text_url', 'translation') !!}</a></li>
                    <li><a href="style-guide.html" title="">{!! $translationService->translate('home::menu.audio_url', 'translation') !!}</a></li>
                    <li><a href="style-guide.html" title="">{!! $translationService->translate('home::menu.video_url', 'translation') !!}</a></li>
                    <li><a href="style-guide.html" title="">{!! $translationService->translate('home::menu.what_next_url', 'translation') !!}</a></li>
                </ul> <!-- end header__nav -->

                <a href="#0" title="Close Menu" class="header__overlay-close close-mobile-menu">Close</a>

            </nav> <!-- end header__nav-wrap -->

        </div> <!-- header-content -->
    </header> <!-- header -->


    <div class="pageheader-content row">
        <div class="col-full">
            <div class="featured">
                @yield('slider')
            </div>
        </div>
    </div>

</section>

