<footer class="s-footer">
    <div class="s-footer__bottom">
        <div class="row">
            <div class="col-full text-center mx-auto">
                {!! setting('site.footer_content')  !!}
            </div>
        </div>
    </div>
</footer>
