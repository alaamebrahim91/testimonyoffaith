<?php

return [
    "dbook" => "تحميل الكتاب",
    "about-book"    => "عن الكتاب",
    "choose-language" => "اختر اللغة",
    "languages" => "اللغات",
    "how-to-participate" => "كيف تشارك",
    "why-book" => "لماذا الكتاب",

];
