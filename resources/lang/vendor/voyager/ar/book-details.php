<?php
/**
 * Copyright (c) 2018. Developed by alaa mohammed
 */

/**
 * Created by PhpStorm.
 * User: alaa
 * Date: 9/15/2018
 * Time: 4:04 PM
 */


return [
    'total-downloads' => 'إجمالى عدد التحميلات',
    "total-downloads-desc" => "إجمالى مرات التحميل لجميع الكتب :count مرة تحميل ، انقر على الزر أدناه للعرض",

];
