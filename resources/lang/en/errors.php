<?php
/**
 * Copyright (c) 2018. Developed by alaa mohammed
 */

/**
 * Created by PhpStorm.
 * User: alaa
 * Date: 9/8/2018
 * Time: 8:13 AM
 */

return [
    "back-to-index" => "Back to main page",
    "404" => "Page not found.",
    "404-description" => "The page you trying to reach is not available right now.",
    "500" => "An error has happened",
    "500-description" => "An unexpected error has happened, if this is a repeatable error please do not hesitate to contact site manager :email"

];
