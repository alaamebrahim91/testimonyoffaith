<?php

return [
    "download-book" => "تحميل الكتاب",
    "about-book"    => "عن الكتاب",
    "choose-language" => "اختر اللغة",
    "languages" => "اللغات",
    "how-to-participate" => "How to participate",
    "why-to-participate" => "Participate"
];
