<?php
/**
 * User: alaa
 * Date: 8/7/2018
 * Time: 5:06 PM
 */

namespace Modules\Admin\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;


class LanguageRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Modules\Admin\Entities\Language';
    }
}
