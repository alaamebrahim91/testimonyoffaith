<?php
/**
 * Copyright (c) 2018. Developed by alaa mohammed
 */

namespace Modules\Admin\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Modules\Book\Services\BookService;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;

class DownloadsDimmer extends BaseDimmer
{

    private $bookService;

    public function __construct(BookService $bookService)
    {
        $this->bookService = $bookService;
    }

    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = $this->bookService->getTotalDownloadsSum();
        $string = trans_choice('voyager::book-details.total-downloads', $count);

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-book-download',
            'title'  => "{$string} {$count}",
            'text'   => __('voyager::book-details.total-downloads-desc', ['count' => $count]),
            'button' => [
                'text' => __('book::generic.dimmer-button'),
                'link' => route('voyager.book-details.index'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/downloads.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', Voyager::model('User'));
    }
}
