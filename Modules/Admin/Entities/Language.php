<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'languages';
    protected $fillable = ['name', 'script', 'native', 'regional', 'key'];

    public function googletranslations() {
        $this->hasMany('google_translations', 'locale_id');
    }
}
