<?php
/**
 * Created by PhpStorm.
 * User: alaa
 * Date: 8/7/2018
 * Time: 5:09 PM
 */

namespace Modules\Admin\Services;

use Illuminate\Support\Facades\App;
use Mcamara\LaravelLocalization\LaravelLocalization;

class SitemapService
{

    public function createSitemap() {
        $this->autoGenerate();
    }


    public function autoGenerate() {
        // create new sitemap object
        $sitemap = App::make("sitemap");

        foreach (config('laravellocalization.supportedLocales') as $code => $props) {
            $sitemap->add(env('APP_URL') . '/' .$code . '/', now(), '0.9');
        }

        // generate your sitemap (format, filename)
        $sitemap->store('xml', 'sitemap');

    }
}
