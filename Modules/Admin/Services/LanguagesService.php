<?php
/**
 * Created by PhpStorm.
 * User: alaa
 * Date: 8/7/2018
 * Time: 5:09 PM
 */

namespace Modules\Admin\Services;

use Modules\Admin\Repositories\LanguageRepository as Language;
use Modules\Admin\Repositories\GoogleTranslationsRepository as Google;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Stichoza\GoogleTranslate\TranslateClient;

class LanguagesService
{

    private $language;
    private $ll;

    public function __construct(Language $language, LaravelLocalization $ll, Google $google)
    {
        $this->language = $language;
        $this->google = $google;
        $this->ll = $ll;

    }

    public function addLanguages()
    {
        foreach (config('laravellocalization.supportedLocales') as $key => $lang) {
            $new = [
                'name' => $lang['name'],
                'script' => $lang['script'],
                'native' => $lang['native'],
                'regional' => $lang['regional'],
                'key' => $key
            ];
            $this->language->create($new);
        }
    }


    /**
     * @param $code
     * @return mixed|string
     * @throws \Exception
     */
    public function translate($code, $type, $database_code = null)
    {
        // If $database_code is not null
        // That means that we are getting translation for a field that is not translation or setting
        // so we put manual code for it
        if($database_code == null){
            $translation = $this->getTranslation($code);
        }else {
            $translation = $this->getTranslation($database_code);
        }

        if(!$translation->isEmpty()) {
            // We got the translation in database
            // We do not need to get translation from google
//            dd($translation);
            return $translation[0]->translation;
        } else {

            try {
                $tr = new TranslateClient();
                $tr->setSource(null); // Translate from ..
                $tr->setTarget($this->ll->getCurrentLocale()); // Translate to ..
                $tr->setUrlBase('http://translate.google.cn/translate_a/single');

                // Get the translation depending on its place
                if($type == 'settings') {
                    $sentence = setting($code);
                } else if ($type == 'translation') {
                    $sentence = trans($code);
                } else if($type == 'database') {
                    $sentence = $code;
                }

                // If i manually set the code in database then i need to use it instead of supplied $code
                if($database_code != null) {
                    $code = $database_code;
                }

                $final_translation = $tr->translate($sentence);
                $this->saveTranslationToDatabase($code, $type, $final_translation);
                return $final_translation;
            } catch (\Exception $e) {
                if($type == 'settings') {
                    $sentence = setting($code);
                } else if ($type == 'translation') {
                    $sentence = trans($code);
                } else if($type == 'database') {
                    $sentence = $code;
                    $code = $database_code;
                }
                return $sentence;
//                throw new \Exception();
            } // Default is from 'auto' to 'en'


        }
    }

    /**
     * @param $code
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function getTranslation($code)
    {
        //code locale_id
        $trans =  $this->google->findByCodeAndLocaleId($code, $this->getLocaleId());
        if($trans) {
            return $trans;
        }
        return null;
    }

    /**
     * @param $code
     * @param $type
     * @param $translation
     */
    public function saveTranslationToDatabase($code, $type, $translation) {
        if($this->getTranslation($code)->isEmpty()) {
            $this->google->create([
                'locale_id'   => $this->getLocaleId(),
                'code'        => $code,
                'type'        => $type,
                'translation' => $translation
            ]);
        }
    }

    /**
     * @return object || null
     */
    public function getLocaleId() {
        $language = $this->language->findBy('key', $this->ll->getCurrentLocale());
        if($language != null) {
            return $language->id;
        } else
            return null;
    }

    public function deleteTranslationByCode($code) {
        $this->google->deleteByCode($code);
    }

    public function deleteTranslationsByCodePart($code) {
        $this->google->deleteByCode($code);
    }

    public function deleteTranslationByType($type) {
        $this->google->deleteByType($type);
    }
}
