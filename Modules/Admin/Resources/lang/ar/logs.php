<?php

return [
    'title' => 'سجل الاخطاء',
    'download' => 'تحميل الملف',
    'clean' => 'تفريغ الملف',
    'delete' => 'حذف الملف',
    'deleteall' => 'حذف الكل',

];
