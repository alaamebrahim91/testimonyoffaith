<?php

namespace Modules\Admin\Http\Controllers\Overrides;

use Illuminate\Support\Facades\Log;
use Modules\Admin\Services\LanguagesService;
use Modules\Home\Services\MainpageService;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use Illuminate\Http\Request;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Facades\Voyager;

class MainpageController extends VoyagerBaseController
{
    /**
     * @var LanguagesService
     */
    private $langService;

    /**
     * @var MainpageService
     */
    private $mainpageService;

    public function __construct(LanguagesService $langService, MainpageService $mainpageService)
    {
        $this->langService = $langService;
        $this->mainpageService = $mainpageService;
    }

    public function update(Request $request, $id)
    {
        $this->deleteTranslations($request->all(), $id);

        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {
            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

            event(new BreadDataUpdated($dataType, $data));

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }

    /**
     * Checks if something has changed and deletes its translations from database
     * @param $data
     * @param $id
     */
    public function deleteTranslations($data, $id){
        $old_data = $this->mainpageService->getSettings();
        try {
            if($old_data->id == $id) {
                if($old_data->title != $data['title']) {
                    $this->langService->deleteTranslationsByCodePart('mainpage.main.title');
                }
                if($old_data->description != $data['description']) {
                    $this->langService->deleteTranslationsByCodePart('mainpage.main.description');
                }
                if($old_data->text_title != $data['text_title']) {
                    $this->langService->deleteTranslationsByCodePart('mainpage.text.title');
                }
                if($old_data->text_description != $data['text_description']) {
                    $this->langService->deleteTranslationsByCodePart('mainpage.text.description');
                }
                if($old_data->audio_title != $data['audio_title']) {
                    $this->langService->deleteTranslationsByCodePart('mainpage.audio.title');
                }
                if($old_data->audio_description != $data['audio_description']) {
                    $this->langService->deleteTranslationsByCodePart('mainpage.audio.description');
                }
                if($old_data->video_title != $data['video_title']) {
                    $this->langService->deleteTranslationsByCodePart('mainpage.video.title');
                }
                if($old_data->video_description != $data['video_description']) {
                    $this->langService->deleteTranslationsByCodePart('mainpage.video.description');
                }
            }
        } catch (\Exception $e) {
            log('Translations not deleted');
            log($e->getMessage());
        }
    }

}
