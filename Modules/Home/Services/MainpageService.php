<?php
/**
 * Created by PhpStorm.
 * User: alaa
 * Date: 8/7/2018
 * Time: 5:09 PM
 */

namespace Modules\Home\Services;

use Modules\Home\Repositories\MainpageRepository as Mainpage;
use Modules\Admin\Repositories\LanguageRepository as Language;
use Modules\Admin\Repositories\GoogleTranslationsRepository as Google;
use Mcamara\LaravelLocalization\LaravelLocalization;

/**
 * Class BookService
 * @package Modules\Book\Services
 */
class MainpageService
{

    /**
     * @var mainpage
     */
    private $mainpage;
    /**
     * @var Language
     */
    private $language;
    /**
     * @var LaravelLocalization
     */
    private $ll;


    public function __construct(Mainpage $mainpage, Language $language, LaravelLocalization $ll, Google $google)
    {
        $this->mainpage = $mainpage;
        $this->language = $language;
        $this->google = $google;
        $this->ll = $ll;
    }


    /**
     * @return void
     */
    public function addLanguages()
    {
        foreach (config('laravellocalization.supportedLocales') as $key => $lang) {
            $new = [
                'name' => $lang['name'],
                'script' => $lang['script'],
                'native' => $lang['native'],
                'regional' => $lang['regional'],
                'key' => $key
            ];
            $this->language->create($new);
        }
    }

    public function getSettings()
    {
        return $this->mainpage->find(1, ['*']);
    }
}
