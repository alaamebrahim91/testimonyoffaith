<?php

Route::group([
    'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'web'],
    'prefix' => LaravelLocalization::setLocale() .'/home',
    'namespace' => 'Modules\Home\Http\Controllers'],
    function()
{
    Route::get('/', 'HomeController@home')->name('index.home');
});
