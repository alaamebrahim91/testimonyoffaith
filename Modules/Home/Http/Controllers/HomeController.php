<?php

namespace Modules\Home\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Home\Services\MainpageService;

class HomeController extends Controller
{
    /**
     * @var MainpageService
     */
    private $mainpageService;

    public function __construct(MainpageService $mainpageService)
    {
        $this->mainpageService = $mainpageService;
    }

    /**
     * Display index page of frontend site
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('theme-home::index');
    }

    /**
     * Display home page after choosing a language
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        $pageData = $this->mainpageService->getSettings();

        return view('theme-home::home', compact(['pageData']));
    }
}
