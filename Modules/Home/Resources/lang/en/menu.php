<?php

return [
    'home_url' => 'Home',
    'text_url' => 'Text',
    'audio_url' => 'Audio',
    'video_url' => 'Video',
    'what_next_url' => 'What next?'
];
