<?php

return [
    'home_url' => 'الصفحة الرئيسية',
    'text_url' => 'نص الشهادة',
    'audio_url' => 'استمع للشهادة',
    'video_url' => 'تعلم بالفيديو',
    'what_next_url' => 'ماذا بعد الشهادة؟'
];
