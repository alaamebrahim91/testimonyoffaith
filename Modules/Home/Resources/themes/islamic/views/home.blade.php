@inject('translationService', 'Modules\Admin\Services\LanguagesService')
@extends('theme::layouts.master')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/home.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
@endsection

@section('javascript')
    {{--<script src="{{ asset('assets/js/home.js') }}"></script>--}}
    <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.slider').bxSlider();
        });
    </script>
@endsection

@section('slider')
    <div class="slider">
        @foreach(json_decode($pageData->slider) as $slider)
            <div>
                <div class="slider-c-mini-container">
                    <img class="slider-img" src="{{ Storage::disk(config('voyager.storage.disk'))->url($slider) }}" alt="">
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('content')

@include('themes.frontend.islamic.views.partials.navbar')

<section class="s-content">

    <div class="row narrow">
        <div class="col-full s-content__header" data-aos="fade-up">
            <h1>{{ $translationService->translate($pageData->title, 'database', 'mainpage.main.title') }}</h1>
            {!! $translationService->translate($pageData->description, 'database', 'mainpage.main.description') !!}
        </div>
    </div>

    <div class="row masonry-wrap">
        <div class="masonry">

            <div class="grid-sizer"></div>

            <article class="masonry__brick entry format-standard" data-aos="fade-up">

                <div class="entry__thumb">
                    <a href="single-standard.html" class="entry__thumb-link">
                        <img src="{{ Storage::disk(config('voyager.storage.disk'))->url($pageData->text_image) }}"
                             srcset="{{ Storage::disk(config('voyager.storage.disk'))->url($pageData->text_image) }} 1x"
                             alt="">
                    </a>
                </div>

                <div class="entry__text">
                    <div class="entry__header">
                        <h1 class="entry__title text-center"><a
                                href="single-standard.html">{{ $translationService->translate($pageData->text_title, 'database', 'mainpage.text.title') }}</a>
                        </h1>

                    </div>
                    <div class="entry__excerpt text-center">
                        {!! $translationService->translate($pageData->text_description, 'database', 'mainpage.text.description')  !!}</p>
                    </div>
                </div>

            </article>

            <article class="masonry__brick entry format-standard" data-aos="fade-up">

                <div class="entry__thumb">
                    <a href="single-standard.html" class="entry__thumb-link">
                        <img src="{{ Storage::disk(config('voyager.storage.disk'))->url($pageData->video_image) }}"
                             srcset="{{ Storage::disk(config('voyager.storage.disk'))->url($pageData->video_image) }} 1x"
                             alt="">
                    </a>
                </div>

                <div class="entry__text">
                    <div class="entry__header">
                        <h1 class="entry__title text-center"><a
                                href="single-standard.html">{{ $translationService->translate($pageData->audio_title, 'database', 'mainpage.video.title') }}</a>
                        </h1>

                    </div>
                    <div class="entry__excerpt text-center">
                        {!! $translationService->translate($pageData->audio_description, 'database', 'mainpage.video.description') !!}
                    </div>

                </div>

            </article>

            <article class="masonry__brick entry format-standard" data-aos="fade-up">

                <div class="entry__thumb">
                    <a href="single-standard.html" class="entry__thumb-link">
                        <img src="{{ Storage::disk(config('voyager.storage.disk'))->url($pageData->audio_image) }}"
                             srcset="{{ Storage::disk(config('voyager.storage.disk'))->url($pageData->audio_image) }} 1x"
                             alt="">
                    </a>
                </div>

                <div class="entry__text">
                    <div class="entry__header">
                        <h1 class="entry__title text-center"><a
                                href="single-standard.html">{{ $translationService->translate($pageData->audio_title, 'database', 'mainpage.audio.title') }}</a>
                        </h1>

                    </div>
                    <div class="entry__excerpt text-center">
                        {!! $translationService->translate($pageData->audio_description, 'database', 'mainpage.audio.description') !!}
                    </div>
                </div>

            </article>

        </div>
    </div>
    <div class="row">
        <div class="col-full mx-auto text-center">
            <a class="btn btn--stroke" href="">{!! $translationService->translate('home::generic.what_next', 'translation') !!}</a>
        </div>
    </div>

</section>

@include('themes.frontend.islamic.views.partials.footer')

@endsection
