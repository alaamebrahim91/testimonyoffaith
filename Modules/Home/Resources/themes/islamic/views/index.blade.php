@inject('bookService', 'Modules\Admin\Services\LanguagesService')
@extends('theme::layouts.master')

@section('main_css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    @if(in_array(LaravelLocalization::getCurrentLocale(), ['ar', 'ur', 'fa']) )
        <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.0.0/css/bootstrap.min.css"
              integrity="sha384-P4uhUIGk/q1gaD/NdgkBIl3a6QywJjlsFJFk7SPRdruoGddvRVSwv5qFnvZ73cpz"
              crossorigin="anonymous">
    @endif
@endsection

@section('javascript')
    <script src="{{ asset('assets/js/index.js') }}"></script>
@endsection

@section('content')
    <div class="container custom-main" style="height: 100%;">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                <img class="mx-auto d-block" src="{{ asset('assets/images/logo.png') }}"
                     class="align-center"/>
            </div>
        </div>
        <div class="row justify-content-center align-items-center">
            <div class="col-12 mx-auto">
                <div class="dropdown mx-auto">
                    <select id="dropdown-c" class="mx-auto">
                        <option value="">{{ $bookService->translate('home::generic.choose_language', 'translation') }}</option>
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <option value="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) . '/home' }}">
                                {{ $properties['native'] }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
@endsection
