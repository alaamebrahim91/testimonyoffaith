const mix = require('laravel-mix');
mix.process
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');


// Copy all main assets to public folder
mix.copyDirectory('resources/views/themes/frontend/islamic/assets', 'public/assets');

//process sass files
mix.sass('Modules/Home/Resources/assets/sass/index.scss', 'public/assets/css').options({processCssUrls: false});
mix.sass('Modules/Home/Resources/assets/sass/home.scss', 'public/assets/css').options({processCssUrls: false});

//process js files
mix.js('Modules/Home/Resources/assets/js/index.js', 'public/assets/js');
